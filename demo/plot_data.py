#!/usr/bin/env python

import plotly.plotly as py      # Every function in this module will communicate with an external plotly server
# import plotly.graph_objs as go
import numpy as np
import sys

f =np.loadtxt(r'data.txt',dtype=str)

x = range(1,len(f)-1)


label = raw_input('Please enter a plot title: ')

py.plot({                      # use `py.iplot` inside the ipython notebook
"data": [{
    # "x": x[42000:58000],
    "x": x,
    "y": f
}],
"layout": {
    "title": label 
}
}, filename=label,      # name of the file as saved in your plotly account
sharing='public')            # 'public' | 'private' | 'secret': Learn more: https://plot.ly/python/privacy

