This code is used to send a matplotlib figure to be plotted on the Plotly website.

Make sure you have a Plotly account configured:
https://plot.ly/python/getting-started/


and that you setup the credentials in python

```python
import plotly 
plotly.tools.set_credentials_file(username='DemoAccount', api_key='lr1c37zw81')
```

Enjoy,
gunnar pope