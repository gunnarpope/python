#!/usr/bin/env python

import plotly.plotly as py      # Every function in this module will communicate with an external plotly server
# import plotly.graph_objs as go
import numpy as np

f =np.loadtxt(r'data.txt',dtype=str)

x = range(1,len(f)-1)

lower = 42000
upper = 58000
diff = upper - lower
fs = 10     #hz
t = np.arange(0,(diff + 1))
t = t/float(fs)

py.plot({                      # use `py.iplot` inside the ipython notebook
"data": [{
    # "x": x[42000:58000],
    "x": t,
    "y": f[42000:58000]
}],
"layout": {
    "title": "Skin Conductance 4/18/2016"
}
}, filename='Skin Conductance 4/18/2016',      # name of the file as saved in your plotly account
sharing='public')            # 'public' | 'private' | 'secret': Learn more: https://plot.ly/python/privacy

