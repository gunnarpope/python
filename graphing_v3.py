#!/usr/bin/env python



# f = open("data.txt","r")

 


import plotly.plotly as py      # Every function in this module will communicate with an external plotly server
# import plotly.graph_objs as go
import numpy as np

#f =np.loadtxt(r'data.txt',dtype=str,delimiter=',',skiprows=1,usecols=(1,))
f =np.loadtxt(r'data.txt',dtype=str)

x = range(1,len(f)-1)

# # Create a trace
# trace = go.Scatter(
#     x = x[1:5000],
#     y = f[1:5000]
# )

# data = [trace]

# # Plot and embed in ipython notebook!
# py.iplot(data, filename='basic-scatter')

# or plot with: plot_url = py.plot(data, filename='basic-line')

py.plot({                      # use `py.iplot` inside the ipython notebook
"data": [{
    "x": x[34000:58000],
    "y": f[34000:58000]
}],
"layout": {
    "title": "Skin Conductance 4/18/2016"
}
}, filename='Skin Conductance 4/18/2016',      # name of the file as saved in your plotly account
sharing='public')            # 'public' | 'private' | 'secret': Learn more: https://plot.ly/python/privacy

