#!/usr/bin/env python

class Car:
    wheels = 4

    def __init__(self,make,model):
        self.make = make
        self.model = model

    def makecarsound(self):
        print "Vroom!!!"
    
    def dosometing(self,do):
        self.make = do

mustang = Car('Ford','Mustang')
print mustang.wheels
print Car.wheels
mustang.dosometing('dodo')
print mustang.make

mustang.makecarsound


